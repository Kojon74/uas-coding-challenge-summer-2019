function pascalsTriangle(n)
{
    if (n%1 != 0 || n < 0){
        return [];
    }
    var nthRow = [n];
    for(var z = 0; z < n; z++){
        nthRow[z] = choose(n-1, z);
    }
    console.log(nthRow);
    return nthRow;
}

function choose(a, b)
{
    if (a == b){
        return 1;
    } else if (b == 0){
        return 1;
    } else {
        return factorial(a)/(factorial(b)*factorial(a-b));
    }
}

function factorial(c)
{
    if (c == 0 || c == 1){
        return 1;
    } else if (c > 1){
        return c*factorial(c-1);
    }
}

pascalsTriangle(1.5);
pascalsTriangle(-2);
pascalsTriangle(3);
pascalsTriangle(4);
pascalsTriangle(5);
pascalsTriangle(6);